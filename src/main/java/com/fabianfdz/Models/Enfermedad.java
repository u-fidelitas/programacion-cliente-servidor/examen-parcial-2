package com.fabianfdz.Models;

/**
 *
 * @author fabianfdz
 */
public class Enfermedad {
    private String nombre, tratamiento;
    private Boolean isConfirmado, isSospechoso;
    private Valoracion valoracion;

    public Enfermedad(String nombre) {
        this.nombre = nombre;
        this.tratamiento = "";
        this.isConfirmado = false;
        this.isSospechoso = true;
    }
    
    public void confirmar() {
        this.isConfirmado = true;
        this.isSospechoso = false;
    }
    
    public void descartar() {
        this.isConfirmado = false;
        this.isSospechoso = false;
    }
    
    public void registrarValoracion(Valoracion valoracion) {
        this.tratamiento = tratamiento;
        this.valoracion = valoracion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Boolean isConfirmado() {
        return this.isConfirmado;
    }
}
