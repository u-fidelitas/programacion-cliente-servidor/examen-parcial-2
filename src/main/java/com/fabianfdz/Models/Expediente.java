package com.fabianfdz.Models;

import java.util.LinkedList;

/**
 *
 * @author fabianfdz
 */
public class Expediente {
    LinkedList<Enfermedad> enfermedades;

    public Expediente() {
        this.enfermedades = new LinkedList<Enfermedad>();
    }
    
    public void registrarEnfermedad(String nombreEnfermedad) {
        enfermedades.add(new Enfermedad(nombreEnfermedad));
    }
    
    public void confirmarEnfermedad(String nombreEnfermedad) {
        enfermedades.forEach((enfermedad) -> {
            if(enfermedad.getNombre().toLowerCase().equals(nombreEnfermedad.toLowerCase())) {
                enfermedad.confirmar();
            }
        });
    }
    
    public void darDeAltaEnfermedad(String nombreEnfermedad) {
        enfermedades.forEach((enfermedad) -> {
            if(enfermedad.getNombre().toLowerCase().equals(nombreEnfermedad.toLowerCase())) {
                enfermedad.descartar();
            }
        });
    }
    
    public void registrarValoracion(String nombreEnfermedad, Valoracion valoracion) {
        enfermedades.forEach((enfermedad) -> {
            if(enfermedad.getNombre().toLowerCase().equals(nombreEnfermedad.toLowerCase())) {
                enfermedad.registrarValoracion(valoracion);
            }
        });
    }
}
