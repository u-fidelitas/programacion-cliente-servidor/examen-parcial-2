package com.fabianfdz.Models;

/**
 *
 * @author fabianfdz
 */
public class Paciente extends Persona {

    double peso, estatura, temperatura;
    int presionSistolica, presionDiastolica;
    Medico medico;
    Expediente expediente;

    public Paciente(double peso, double estatura, double temperatura, int presionSistolica, int presionDiastolica, int id, String nombre, String apellido1, String apellido2, String genero, String email, String fechaNacimiento, String telefono, String provincia) {
        super(id, nombre, apellido1, apellido2, genero, email, fechaNacimiento, telefono, provincia);
        this.peso = peso;
        this.estatura = estatura;
        this.temperatura = temperatura;
        this.presionSistolica = presionSistolica;
        this.presionDiastolica = presionDiastolica;
        this.expediente = new Expediente();
    }

    public void registrarMedico(Medico medico) {
        this.medico = medico;
    }

    public void registrarEnfermedad(String nombre) {
        this.expediente.registrarEnfermedad(nombre);
    }

    public void registrarValoracion(String nombreEnfermedad, Valoracion valoracion, boolean resultado) {
        this.expediente.registrarValoracion(nombreEnfermedad, valoracion);
        if (resultado) {
            this.expediente.confirmarEnfermedad(nombreEnfermedad);
        } else {
            this.expediente.darDeAltaEnfermedad(nombreEnfermedad);
        }
    }

    public boolean isInfectado() {
        for (Enfermedad enfermedad : this.expediente.enfermedades) {
            if (enfermedad.isConfirmado()) {
                return true;
            }
        }

        return false;
    }
    
    public String getFirstEnfermedad() {
        return this.expediente.enfermedades.getFirst().getNombre();
    }
}
