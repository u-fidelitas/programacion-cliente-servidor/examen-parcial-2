package com.fabianfdz.Models;

/**
 *
 * @author fabianfdz
 */
public class Medico extends Persona{
    
    int codigoMed;
    String especialidad;
    boolean isIndependiente;

    public Medico(int codigoMed, String especialidad, Boolean isIndependiente, int id, String nombre, String apellido1, String apellido2, String genero, String email, String fechaNacimiento, String telefono, String provincia) {
        super(id, nombre, apellido1, apellido2, genero, email, fechaNacimiento, telefono, provincia);
        this.codigoMed = codigoMed;
        this.especialidad = especialidad;
        this.isIndependiente = isIndependiente;
    }
}
