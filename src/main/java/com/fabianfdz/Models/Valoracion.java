package com.fabianfdz.Models;

/**
 *
 * @author fabianfdz
 */
public class Valoracion {
    String valorador, indicaciones;
    boolean isMedico;

    public Valoracion(String valorador, String indicaciones, boolean isMedico) {
        this.valorador = valorador;
        this.isMedico = isMedico;
        this.indicaciones = indicaciones;
    }
}
