package com.fabianfdz.Models;

/**
 *
 * @author fabianfdz
 */
public class Persona {
    
    int id;
    String nombre, apellido1, apellido2, genero, email, fechaNacimiento, telefono, provincia;

    public Persona(int id, String nombre, String apellido1, String apellido2, String genero, String email, String fechaNacimiento, String telefono, String provincia) {
        this.id = id;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.genero = genero;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
        this.telefono = telefono;
        this.provincia = provincia;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getProvincia() {
        return provincia;
    }

    @Override
    public String toString() {
        return "\t" + this.id + "\t" + this.apellido1 + " " + this.apellido2 + ", " + this.nombre; //To change body of generated methods, choose Tools | Templates.
    }
}
