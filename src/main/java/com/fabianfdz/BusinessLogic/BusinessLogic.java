package com.fabianfdz.BusinessLogic;

import com.fabianfdz.Models.Medico;
import com.fabianfdz.Models.Paciente;
import com.fabianfdz.Models.Valoracion;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.LinkedList;

/**
 *
 * @author fabianfdz
 */
public class BusinessLogic {

    private static LinkedList<Medico> medicos = new LinkedList<Medico>();
    private static LinkedList<Paciente> pacientes = new LinkedList<Paciente>();

    private static final int MAX_MEDICOS = 50;
    private static final int MAX_PACIENTES = 5000;
    private static final int HAB_CR = 5000000;
    private static final String PATH_DB_PACIENTES = "./db_pacientes.csv";
    private static final String PATH_DB_MEDICOS = "./db_medicos.csv";
    private static final String SEPARATOR_FILE = "%%%";

    /* REGISTRAR MEDICO */
    public static void registrarMedico(int codigoMed, String especialidad,
            boolean isIndependiente, int id, String nombre, String apellido1,
            String apellido2, String genero, String email, String fechaNacimiento,
            String telefono, String provincia, boolean save) throws IOException {
        if (isMaxMedicos()) {
            throw new Error("Se ha llegado al máximo numero de médicos");
        }

        Medico newMedico = new Medico(codigoMed, especialidad,
                isIndependiente, id, nombre, apellido1, apellido2,
                genero, email, fechaNacimiento, telefono, provincia);

        if (!isMedicoRegistrado(newMedico.getId())) {
            medicos.add(newMedico);
            if (save) {
                try {
                    addMedicosDB(codigoMed, especialidad, isIndependiente, id, nombre, apellido1, apellido2, genero, email, fechaNacimiento, telefono, provincia);
                } catch (Error err) {
                    throw new Error("Error guardando paciente en DB.");
                }
            }
        } else {
            throw new Error("Ya el médico con el ID " + id + " se encuentra registrado");
        }
    }

    private static boolean isMedicoRegistrado(int id) {
        for (Medico medico : medicos) {
            if (medico.getId() == id) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMaxMedicos() {
        return medicos.size() > MAX_MEDICOS;
    }

    /* REGISTRAR PACIENTE */
    public static void registrarPaciente(double peso, double estatura, double temperatura,
            int presionSistolica, int presionDiastolica, int idMedico, int id, String nombre,
            String apellido1, String apellido2, String genero, String email,
            String fechaNacimiento, String telefono, String provincia, String nombreEnfermedadInicial, boolean save) throws IOException {
        if (isMaxPacientes()) {
            throw new Error("Se ha llegado al máximo numero de pacientes");
        }

        Paciente newPaciente = new Paciente(peso, estatura, temperatura, presionSistolica,
                presionDiastolica, id, nombre, apellido1, apellido2, genero, email,
                fechaNacimiento, telefono, provincia);

        if (!isPacienteRegistrado(newPaciente.getId())) {
            Medico medico = getMedicoById(idMedico);
            newPaciente.registrarEnfermedad(nombreEnfermedadInicial);
            newPaciente.registrarMedico(medico);
            pacientes.add(newPaciente);
            if (save) {
                try {
                    addPacienteSospechosoDB(peso, estatura, temperatura, presionSistolica, presionDiastolica, idMedico, id, nombre, apellido1, apellido2, genero, email, fechaNacimiento, telefono, provincia, nombreEnfermedadInicial);
                } catch (Error err) {
                    throw new Error("Error guardando paciente en DB.");
                }
            }
        } else {
            throw new Error("Ya el paciente con el id " + id + " se encuentra registrado");
        }
    }

    private static boolean isPacienteRegistrado(int id) {
        for (Paciente paciente : pacientes) {
            if (paciente.getId() == id) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMaxPacientes() {
        return pacientes.size() > MAX_PACIENTES;
    }

    private static Medico getMedicoById(int id) {
        for (Medico medico : medicos) {
            if (medico.getId() == id) {
                return medico;
            }
        }
        throw new Error("No hay un médico con el ID proporcionado");
    }

    /* REGISTRAR RESULTADO */
    public static void registrarResultadoPorMedico(int idMedico, int idPaciente, String indicaciones, boolean resultado) {
        for (Paciente paciente : pacientes) {
            if (paciente.getId() == idPaciente) {
                if (resultado) {
                    Medico medico = getMedicoById(idMedico);
                    registrarValoracion(paciente, medico.getNombre(), true, indicaciones, true);
                    return;
                } else {
                    darDeAltaPaciente(paciente);
                    return;
                }
            }
        }
    }

    public static void registrarResultadoPorOtro(String valorador, int idPaciente, String indicaciones, boolean resultado) {
        for (Paciente paciente : pacientes) {
            if (paciente.getId() == idPaciente) {
                if (resultado) {
                    registrarValoracion(paciente, valorador, false, indicaciones, true);
                    return;
                } else {
                    darDeAltaPaciente(paciente);
                    return;
                }
            }
        }
    }

    private static void registrarValoracion(Paciente paciente, String nombreValorador, boolean isMedico, String indicacionesMedico, boolean resultado) {
        Valoracion valoracion = new Valoracion(nombreValorador, indicacionesMedico, isMedico);
        paciente.registrarValoracion(paciente.getFirstEnfermedad(), valoracion, resultado);
    }

    private static void darDeAltaPaciente(Paciente paciente) {
        pacientes.remove(paciente);
    }

    /* CASOS POSITIVOS X PROVINCIA */
    public static String casosPositivosPorPrivincia() {
        String result = "";

        LinkedList<String> provinciasInfectadas = getProvinciasInfectadas();

        for (String provincia : provinciasInfectadas) {
            result += "Provincia " + provincia.substring(0, 1).toUpperCase()
                    + provincia.substring(1).toLowerCase() + ":\n";
            LinkedList<Paciente> pacientesEnProvincia = getPacientesPorProvincia(provincia);
            for (Paciente paciente : pacientesEnProvincia) {
                result += paciente.toString() + "\n";
            }
            result += "\n\n";
        }

        return result;
    }

    private static LinkedList<Paciente> getPacientesPorProvincia(String provincia) {
        LinkedList<Paciente> pacientesProv = new LinkedList<Paciente>();
        for (Paciente paciente : pacientes) {
            if ((paciente.getProvincia().toLowerCase().contains(provincia.toLowerCase())) && paciente.isInfectado()) {
                pacientesProv.add(paciente);
            }
        }

        return pacientesProv;
    }

    private static LinkedList<String> getProvinciasInfectadas() {
        LinkedList<String> provincias = new LinkedList<String>();
        for (Paciente paciente : pacientes) {
            if (paciente.isInfectado() && !provincias.contains(paciente.getProvincia())) {
                provincias.add(paciente.getProvincia());
            }
        }

        return provincias;
    }

    /* % TICOS CON VIRUS */
    public static double getPorcentajeInfectados() {
        return (getCantidadHabInf() / getCantidadHab()) * 100;
    }
    
    public static int getCantidadHab() {
        return HAB_CR;
    }

    public static int getCantidadHabInf() {
        int infectados = 0;

        for (Paciente paciente : pacientes) {
            if (paciente.isInfectado()) {
                infectados++;
            }
        }

        return infectados;
    }
    /* LISTA MEDICOS */
    public static String[] getMedicos() {
        String[] nombresMedicos = new String[medicos.size()];
        int idx = 0;
        for (Medico medico : medicos) {
            nombresMedicos[idx++] = medico.toString().trim();
        }

        return nombresMedicos;
    }

    /* LISTA PACIENTES */
    public static String[] getPacientesContainst(String textSearch) {
        String[] nombresPacientes = new String[pacientes.size()];
        int idx = 0;
        for (Paciente paciente : pacientes) {
            if (paciente.toString().toLowerCase().contains(textSearch.toLowerCase()) && !paciente.isInfectado()) {
                nombresPacientes[idx++] = paciente.toString().trim();
            }
        }

        return nombresPacientes;
    }

    /* CANTIDADES */
    public static int pacientesLen() {
        return pacientes.size();
    }

    public static int medicosLen() {
        return medicos.size();
    }

    /* CSV "DB" */
    public static void getPacientesDB() throws IOException {
        File db = new File(PATH_DB_PACIENTES);

        if (!db.exists()) {
            db.createNewFile(); //creating it
        } else {
            BufferedReader csvReader = new BufferedReader(new FileReader(db));
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(SEPARATOR_FILE);
                registrarPaciente(Double.parseDouble(data[0]), Double.parseDouble(data[1]),
                        Double.parseDouble(data[2]), Integer.parseInt(data[3]), Integer.parseInt(data[4]),
                        Integer.parseInt(data[5]), Integer.parseInt(data[6]), data[7], data[8],
                        data[9], data[10], data[11], data[12], data[13], data[14], data[15], false);
            }
            csvReader.close();
        }
    }

    public static void getMedicosDB() throws IOException {
        File db = new File(PATH_DB_MEDICOS);

        if (!db.exists()) {
            db.createNewFile(); //creating it
        } else {
            try ( BufferedReader csvReader = new BufferedReader(new FileReader(db))) {
                String row;
                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(SEPARATOR_FILE);
                    registrarMedico(Integer.parseInt(data[0]), data[1], Boolean.valueOf(data[2]),
                            Integer.parseInt(data[3]), data[4], data[5], data[6], data[7],
                            data[8], data[9], data[10], data[11], false);
                }
            }
        }
    }

    public static void addMedicosDB(int codigoMed, String especialidad,
            boolean isIndependiente, int id, String nombre, String apellido1,
            String apellido2, String genero, String email, String fechaNacimiento,
            String telefono, String provincia) throws IOException {
        try ( FileWriter csvWriter = new FileWriter(PATH_DB_MEDICOS, true)) {
            csvWriter.append(String.valueOf(codigoMed));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(especialidad);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(isIndependiente));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(id));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(nombre);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(apellido1);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(apellido2);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(genero);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(email);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(fechaNacimiento);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(telefono);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(provincia);
            csvWriter.append("\n");
        }
    }

    public static void addPacienteSospechosoDB(double peso, double estatura, double temperatura,
            int presionSistolica, int presionDiastolica, int idMedico, int id, String nombre,
            String apellido1, String apellido2, String genero, String email,
            String fechaNacimiento, String telefono, String provincia, String nombreEnfermedadInicial) throws IOException {
        try ( FileWriter csvWriter = new FileWriter(PATH_DB_PACIENTES, true)) {
            csvWriter.append(String.valueOf(peso));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(estatura));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(temperatura));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(presionSistolica));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(presionDiastolica));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(idMedico));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(String.valueOf(id));
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(nombre);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(apellido1);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(apellido2);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(genero);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(email);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(fechaNacimiento);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(telefono);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(provincia);
            csvWriter.append(SEPARATOR_FILE);
            csvWriter.append(nombreEnfermedadInicial);
            csvWriter.append("\n");
        }
    }

}
