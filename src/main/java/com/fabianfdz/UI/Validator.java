package com.fabianfdz.UI;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author fabianfdz
 */
public class Validator {

    private static boolean verifyEmail(String email) {
        String regex = "^(.+)@(.+)$";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String getEmail(String email) {
        if (verifyEmail(email)) {
            return email;
        } else {
            throw new Error("Ingrese un correo válido");
        }
    }

    public static double getDouble(Object fieldValue, String fieldName) {
        String valueStr = String.valueOf(fieldValue);
        if (valueStr.equals("")) {
            throw new Error("Ingrese un valor en el campo " + fieldName);
        }
        try {
            return Double.parseDouble(valueStr);
        } catch (Error err) {
            throw new Error("Ingrese números en el campo " + fieldName);
        }
    }

    public static int getInt(Object fieldValue, String fieldName) {
        String valueStr = String.valueOf(fieldValue);
        if (valueStr.equals("")) {
            throw new Error("Ingrese un valor en el campo " + fieldName);
        }
        try {
            return Integer.parseInt(String.valueOf(valueStr));
        } catch (Error err) {
            throw new Error("Ingrese números en el campo " + fieldName);
        }
    }
    
    public static String getText(String fieldValue, String fieldName) {
        if (fieldValue.equals("")) {
            throw new Error("Ingrese un valor en el campo " + fieldName);
        }
        
        return fieldValue;
    }

}
